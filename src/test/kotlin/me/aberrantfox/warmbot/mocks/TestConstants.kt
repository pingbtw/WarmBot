package me.aberrantfox.warmbot.mocks

object TestConstants {
    //Configuration defaults
    const val Guild_ID = "insert-id"
    const val Guild_Name = "test-guild"
    const val Category_ID = "category-id"
    const val Channel_ID = "channel-id"
    const val Staff_Role = "staff-name"

    //Mock defaults
    const val User_ID = "user-id"
    const val Message_ID = "message-id"

    //Macro defaults
    const val Macro_Name = "macro-name"
    const val Macro_Message = "macro-message"
}